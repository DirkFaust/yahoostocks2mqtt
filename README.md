# WIP: Yahoo finance stock quotes to mqtt service
The mqtt part is still going out to own mod when finished

### ENV
#### QUOTES
Comma-separated array of Yahoo-Symbols that should be queried. Example: "VGWL.DE, VFEM.DE, NLLSF, PFE.DE"

#### PERIOD
Human readable frequency to poll quotes. Example: 5m

#### MQTT_HOST
Host of the MQTT server to report to. Example: 192.168.1.104

#### MQTT_PORT
Port of the MQTT server to report to. Example: 1883
