module gitlab.com/DirkFaust/yahoostocks2mqtt

go 1.18

require (
	github.com/go-resty/resty/v2 v2.7.0
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/DirkFaust/faustility v0.0.0-20220623095737-3c0e31ac2975
)

require (
	github.com/goiiot/libmqtt v0.9.6 // indirect
	github.com/klauspost/compress v1.15.6 // indirect
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d // indirect
	golang.org/x/sys v0.0.0-20220608164250-635b8c9b7f68 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
