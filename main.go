package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/DirkFaust/faustility/pkg/env"
	"gitlab.com/DirkFaust/faustility/pkg/mqtt"
	"gitlab.com/DirkFaust/yahoostocks2mqtt/pkg/yahoo"
)

const (
	envQuotes   = "QUOTES"
	envPeriod   = "PERIOD"
	envMqttHost = "MQTT_HOST"
	envMqttPort = "MQTT_PORT"
)

func main() {
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	quotesToRequest := env.GetOrDie(envQuotes)
	period := env.GetOrDie(envPeriod)
	mqttHost := env.GetOrDie(envMqttHost)
	mqttPortStr := os.Getenv(envMqttPort)

	if len(quotesToRequest) == 0 || len(period) == 0 || len(mqttHost) == 0 {
		panic("Required ENV(s) not found.")
	}

	if len(mqttPortStr) == 0 {
		// Default
		mqttPortStr = "1883"
	}

	requestPeriod, err := time.ParseDuration(period)
	if err != nil {
		panic("Unable to decode the give PERIOD ENV")
	}

	mqttPort, err := strconv.Atoi(mqttPortStr)
	if err != nil {
		panic(fmt.Sprintf("Unableto convert the given port to a numeric value: %v", err))
	}

	symbols := strings.Split(quotesToRequest, ",")
	for i := 0; i < len(symbols); i++ {
		symbols[i] = strings.TrimSpace(symbols[i])
	}

	log.Infof("Requesting quotes: %v", symbols)

	if len(symbols) == 0 {
		panic("Unable to exctract symbols from 'QUOTE' ENV")
	}

	quotes := make(chan yahoo.YahooStockInfo)
	kill := make(chan bool)

	forwarder, err := mqtt.NewForwarderBuilder[yahoo.YahooStockInfo]().
		Channels().Data(quotes).KillWitch(kill).
		Topics().Input("yahoo2mqtt/command").Output("yahoo2mqtt/quote").
		Server().Host(mqttHost).Port(uint16(mqttPort)).
		Build()

	if err != nil {
		log.Errorf("Unable to build MQTT forwarder: %v", err)
		os.Exit(1)
	}

	go yahoo.Task(symbols, requestPeriod, quotes, kill)

	forwarder.Run()
}
