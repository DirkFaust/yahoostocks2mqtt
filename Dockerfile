FROM alpine:latest

ARG TARGETOS
ARG TARGETARCH

RUN echo "Using binary: ./build_${TARGETOS}_${TARGETARCH}/yahoostocks2mqtt"

COPY ./build_${TARGETOS}_${TARGETARCH}/yahoostocks2mqtt /opt/

ENV QUOTES="VGWL.DE, VFEM.DE, NLLSF, PFE.DE"
ENV PERIOD="5m"
ENV MQTT_HOST="192.168.1.104"
ENV MQTT_PORT="1883"

USER 1001

CMD /opt/yahoostocks2mqtt
