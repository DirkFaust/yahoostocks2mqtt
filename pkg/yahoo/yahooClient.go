package yahoo

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
	log "github.com/sirupsen/logrus"
)

const VARIANCE_SECONDS_MIN = 0
const VARIANCE_SECONDS_MAX = 30

// TODO: isn't there any equivalent to serde:rename_all(camelCase)?
type YahooStockInfo struct {
	QuoteType          string  `json:"quoteType"`
	Currency           string  `json:"currency"`
	RegularMarketPrice float64 `json:"regularMarketPrice"`
	Symbol             string  `json:"symbol"`
}

func (stock YahooStockInfo) Serialize() (*[]byte, error) {
	result, err := json.Marshal(stock)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

type YahooQuoteResponse struct {
	Result []YahooStockInfo `json:"result"`
}

type YahooResponse struct {
	QuoteResponse YahooQuoteResponse `json:"quoteResponse"`
}

func RequestSymbols(symbols []string) ([]YahooStockInfo, error) {
	symbolsList := strings.Join(symbols, ",")

	client := resty.New()
	resp, err := client.R().
		SetQueryParams(map[string]string{
			"symbols": symbolsList,
		}).
		SetHeader("accept", "application/json").
		SetHeader("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36").
		Get("https://query2.finance.yahoo.com/v7/finance/quote")

	if err != nil {
		return nil, fmt.Errorf("unable to request quotes: %v", err)
	}

	var yahooResponse YahooResponse
	uerr := json.Unmarshal(resp.Body(), &yahooResponse)
	if uerr != nil {
		return nil, fmt.Errorf("unable to unmarshal quotes response: %v -> '%v'", uerr, string(resp.Body()))
	}

	return yahooResponse.QuoteResponse.Result, nil
}

// Sleeps the given `duration` and signals the completion via the sleepDone channel
func sleeper(ctx context.Context, duration time.Duration, sleepDone chan bool) {
	timer := time.NewTimer(duration)
	select {
	case <-ctx.Done():
		log.Debug("Timer done")
		if !timer.Stop() {
			log.Info("Timer could not be stopped. Draining.")
			<-timer.C
		}
	case <-timer.C:
	}
	log.Debug("Timer signalling finish")
	sleepDone <- true
}

func Task(symbols []string, requestPeriod time.Duration, quotesChannel chan YahooStockInfo, kill chan bool) {
	rand.Seed(time.Now().UnixNano())
	sleepDone := make(chan bool)
	for {
		seconds := (rand.Intn(VARIANCE_SECONDS_MAX-VARIANCE_SECONDS_MIN) + VARIANCE_SECONDS_MIN) * int(time.Second)
		variance := time.Duration(seconds)

		quotes, err := RequestSymbols(symbols)
		if err != nil {
			log.Fatalf("unable to request symbols: %v", err)
		} else {
			log.Debugf("Got quotes: %v", quotes)
			for _, quote := range quotes {
				quotesChannel <- quote
			}
		}

		ctx, cancel := context.WithCancel(context.Background())
		log.Debugf("Sleeping for %v", requestPeriod+variance)
		go sleeper(ctx, requestPeriod+variance, sleepDone)
		select {
		case <-sleepDone:
			continue
		case <-kill:
			log.Info("Exiting requestLoop for killswitch, canceling the sleep for next request")
			cancel()
			return
		}
	}
}
